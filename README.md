# bee-yards-registry

Simple CRUD-based application to managing bee yards registry.

Application was divided into two main folders, backend and frontend.

To go to frontend:

```bash
cd frontend
```

To go to backend:

```bash
cd backend
```

## Backend

Tech stack: Node.js, Express.js, knex.js, typescript

Database: PostgreSQL

It is required to have set up database with default values declared in config/default.ts

It is required to have installed all dependecies required in package.json

To setup backend correctly, you have to install:

[node](https://nodejs.org/download/release/v14.17.0/)

[postgresql](https://www.postgresql.org/download/)

Then, use

```bash
psql - U or create database (i.e in pgAdmin)
```

to create database.

Then go to backend folder, and use

```bash
npm install
```

Next, you have to use

```bash
knex migrate:latest
```

to migrate all tables to your database.

After that use

```bash
npm run build
```

to compile typescript code to javascript.

And then

```bash
npm run start
```

to run backend.

## Frontend

Tech stack: Next.js, React

It is required to have installed all dependecies required in package.json

First, you have to use:

```bash
npm install
```

to install all required dependecies.

Then use

```
npm run dev
```

to start an app in developement mode.

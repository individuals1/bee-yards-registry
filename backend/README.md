# bee-yards-registry

## Database migration

[knex migrations documentation](http://knexjs.org/#Migrations)

### Creating a migration

```bash
knex migrate:make [migration_name]
```

### Running a migration

```bash
knex migrate:up [optional migration name]
```

### Rollbacking migration

```bash
knex migration:down [optional migration name]
```

## Running tests

```bash
npm run test
```

## Building typescript code

```bash
npm run build
```

## Running an app

```bash
npm start
```

## Format and rebuild docs from yaml to json (windows)

```bash
npm run build-docs-win
```

import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("bee_yards", (table) => {
    table.increments("id").primary();
    table.string("name");
    table.date("date");
    table.string("register_number").unique();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("bee_yards");
}

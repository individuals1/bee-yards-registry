import { knex } from "@helpers/knex.helper";
import BeeYard, {
  BeeYardInput,
  BeeYardModelInput,
} from "@interfaces/bee-yard.interface";

const BeeYardModel = {
  create: (beeYardInput: BeeYardModelInput): Promise<BeeYard> => {
    return knex("bee_yards").insert(beeYardInput).returning("*").getFirst();
  },
  find: (id: number): Promise<BeeYard> => {
    return knex("bee_yards").select().where({ id }).getFirst();
  },

  findAllByDate: (date: Date): Promise<BeeYard[]> => {
    return knex("bee_yards").select().where({ date });
  },

  search: (startDate: Date, endDate: Date): Promise<BeeYard[]> => {
    const beeYards = knex("bee_yards").select();

    if (startDate) {
      beeYards.andWhere("date", ">=", startDate);
    }

    if (endDate) {
      beeYards.andWhere("date", "<=", endDate);
    }

    return beeYards;
  },

  update: (
    id: number,
    beeYardInput: Partial<BeeYardModelInput>
  ): Promise<BeeYard> => {
    return knex("bee_yards")
      .update(beeYardInput)
      .where({ id })
      .returning("*")
      .getFirst();
  },

  delete: (id: number): Promise<BeeYard> => {
    return knex("bee_yards").del().where({ id }).returning("*").getFirst();
  },
};

export default BeeYardModel;

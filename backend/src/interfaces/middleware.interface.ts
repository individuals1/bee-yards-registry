import { Result, ValidationChain } from "express-validator";
import { Middleware } from "express-validator/src/base";
import { Request, Response } from "express";

export default interface Wrapper {
  validators:
    | ValidationChain[]
    | (Middleware & { run: (req: Request) => Promise<Result<any>> })[];
  controller: (req: Request, res: Response) => void;
}

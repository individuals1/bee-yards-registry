export interface BeeYardInput {
  name: string;
  date: Date;
  serialNumber?: string;
}

export interface BeeYardModelInput
  extends Omit<BeeYardInput, "additionalRegisterNumber"> {
  registerNumber: string;
}

export default interface BeeYard extends BeeYardModelInput {
  id: number;
}

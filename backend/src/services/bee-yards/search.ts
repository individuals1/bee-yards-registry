import BeeYard from "@interfaces/bee-yard.interface";
import BeeYardModel from "@models/bee-yard.model";

export default async (startDate: Date, endDate: Date): Promise<BeeYard[]> => {
  return BeeYardModel.search(startDate, endDate);
};

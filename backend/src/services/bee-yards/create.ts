import BeeYard, { BeeYardInput } from "@interfaces/bee-yard.interface";
import BeeYardModel from "@models/bee-yard.model";

import { BeeYardCreateError } from "./error";
import { generateRegisterNumber } from "./helper";

export default async (beeYardInput: BeeYardInput): Promise<BeeYard> => {
  const { serialNumber, ...partialBeeYardInput } = beeYardInput;

  const registerNumber = await generateRegisterNumber(
    partialBeeYardInput.date,
    serialNumber
  );

  const newBeeYard = await BeeYardModel.create({
    ...partialBeeYardInput,
    registerNumber,
  });

  if (!newBeeYard) throw new BeeYardCreateError();

  return newBeeYard;
};

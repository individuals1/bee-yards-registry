import BeeYard, { BeeYardInput } from "@interfaces/bee-yard.interface";
import BeeYardModel from "@models/bee-yard.model";

import { BeeYardNotFound, BeeYardUpdateError } from "./error";

export default async (
  beeYardId: number,
  beeYardInput: BeeYardInput
): Promise<BeeYard> => {
  const foundBeeYard = await BeeYardModel.find(beeYardId);

  if (!foundBeeYard) throw new BeeYardNotFound();

  const updatedBeeYard = await BeeYardModel.update(beeYardId, beeYardInput);

  if (!updatedBeeYard) throw new BeeYardUpdateError();

  return updatedBeeYard;
};

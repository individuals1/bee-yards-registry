import BEE_YARD_SETTINGS from "@enums/bee-yard-settings.enum";
import BaseError from "@exceptions/base.error";

const SERVICE = "BeeYardsService";

export class BeeYardNotFound extends BaseError {
  constructor() {
    super("Bee yard was not found", SERVICE, 404);
  }
}

export class BeeYardCreateError extends BaseError {
  constructor() {
    super("Bee yard was not created", SERVICE);
  }
}

export class BeeYardDeleteError extends BaseError {
  constructor() {
    super("Bee yard was not deleted", SERVICE);
  }
}

export class BeeYardUpdateError extends BaseError {
  constructor() {
    super("Bee yard was not updated", SERVICE);
  }
}

export class ExceedsAllowedSerialNumber extends BaseError {
  constructor() {
    super(
      `Bee yard registry serial number exceeds max allowed daily serial number of registry: ${BEE_YARD_SETTINGS.MAX_ALLOWED_SERIAL_NUMBER}`,
      SERVICE
    );
  }
}

export class RegistryNumberAlreadyExists extends BaseError {
  constructor() {
    super("Bee yard registry number already exists", SERVICE, 409);
  }
}

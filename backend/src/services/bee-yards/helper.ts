import { format, parse } from "date-fns";
import bigInt from "big-integer";
import BeeYardModel from "@models/bee-yard.model";
import BEE_YARD_SETTINGS from "@enums/bee-yard-settings.enum";
import BeeYard from "@interfaces/bee-yard.interface";

import {
  ExceedsAllowedSerialNumber,
  RegistryNumberAlreadyExists,
} from "./error";

const parseDate = (date): string => {
  if (!date.includes("-")) return null;

  return format(
    parse(date, BEE_YARD_SETTINGS.DEFAULT_DATE_FORMAT, new Date()),
    BEE_YARD_SETTINGS.PARSED_DATE_FORMAT
  );
};

const calculateChecksum = (registerNumber: string): string => {
  const parsedRegisterNumber = parseInt(registerNumber);

  const digitsToProduct = [];

  for (const digit of registerNumber) {
    if (parseInt(digit) !== 0) digitsToProduct.push(parseInt(digit));
  }

  let calculatedSum;
  let calculatedSums = [];

  for (const digit of digitsToProduct) {
    if (!calculatedSum) {
      calculatedSum = bigInt(parsedRegisterNumber.toString()).multiply(digit);
    } else {
      calculatedSum = bigInt(calculatedSum.toString()).multiply(digit);
    }

    calculatedSums.push(calculatedSum);
  }

  const parsedCalculatedSum =
    calculatedSums[calculatedSums.length - 1].value.toString();

  return (
    parsedCalculatedSum.charAt(BEE_YARD_SETTINGS.SECOND_CHECKSUM_NUMBER_INDEX) +
    parsedCalculatedSum.charAt(
      BEE_YARD_SETTINGS.SEVENTH_CHECKSUM_NUMBER_INDEX
    ) +
    parsedCalculatedSum.slice(BEE_YARD_SETTINGS.LAST_CHECKSUM_NUMBER_INDEX)
  );
};

const getAllDailySerialNumbers = (beeYardsRegisters: BeeYard[]): string[] => {
  let allDailySerialNumbers = [];

  for (const beeYardRegister of beeYardsRegisters) {
    const serialNumber = beeYardRegister.registerNumber.substring(
      BEE_YARD_SETTINGS.SERIAL_NUMBER_START_INDEX,
      BEE_YARD_SETTINGS.SERIAL_NUMBER_END_INDEX
    );
    allDailySerialNumbers.push(serialNumber);
  }

  return allDailySerialNumbers;
};

const generateSerialNumber = (beeYardsRegisters: BeeYard[]): string => {
  const allDailySerialNumbers = getAllDailySerialNumbers(beeYardsRegisters);

  let serialNumber = BEE_YARD_SETTINGS.FIRST_DAILY_SERIAL_NUMBER.toString();

  while (allDailySerialNumbers.includes(serialNumber)) {
    serialNumber = (parseInt(serialNumber) + 1)
      .toString()
      .padStart(BEE_YARD_SETTINGS.SERIAL_NUMBER_LENGTH, "0");
  }

  if (serialNumber.length > BEE_YARD_SETTINGS.SERIAL_NUMBER_LENGTH)
    throw new ExceedsAllowedSerialNumber();

  return serialNumber;
};

const checkSerialNumber = (
  beeYardsRegisters: BeeYard[],
  optionalSerialNumber: string
): void => {
  const allDailySerialNumbers = getAllDailySerialNumbers(beeYardsRegisters);

  if (allDailySerialNumbers.includes(optionalSerialNumber)) {
    throw new RegistryNumberAlreadyExists();
  }

  return;
};

export const generateRegisterNumber = async (
  date: Date,
  optionalSerialNumber: string | null
): Promise<string> => {
  const parsedDate = parseDate(date);
  const beeYardsRegisters = await BeeYardModel.findAllByDate(date);

  let registerNumber;

  if (optionalSerialNumber) {
    if (beeYardsRegisters.length) {
      checkSerialNumber(beeYardsRegisters, optionalSerialNumber);
    }

    registerNumber = `${parsedDate}${optionalSerialNumber}`;
    const checksum = calculateChecksum(registerNumber);

    return `${registerNumber}${checksum}`;
  }

  const generatedNumber = !beeYardsRegisters.length
    ? BEE_YARD_SETTINGS.FIRST_DAILY_SERIAL_NUMBER
    : generateSerialNumber(beeYardsRegisters);

  registerNumber = `${parsedDate}${generatedNumber}`;
  const checksum = calculateChecksum(registerNumber);

  return `${registerNumber}${checksum}`;
};

export const functionsToTest = {
  checkSerialNumber,
  generateSerialNumber,
  getAllDailySerialNumbers,
  calculateChecksum,
  parseDate,
};

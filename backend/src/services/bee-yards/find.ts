/** @format */

import BeeYard from "@interfaces/bee-yard.interface";
import BeeYardModel from "@models/bee-yard.model";

import { BeeYardNotFound } from "./error";

export default async (beeYardId: number): Promise<BeeYard> => {
  const foundBeeYard = await BeeYardModel.find(beeYardId);

  if (!foundBeeYard) throw new BeeYardNotFound();

  return foundBeeYard;
};

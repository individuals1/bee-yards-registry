import BeeYardModel from "@models/bee-yard.model";

import { BeeYardDeleteError, BeeYardNotFound } from "./error";

export default async (beeYardId: number): Promise<void> => {
  const foundBeeYard = await BeeYardModel.find(beeYardId);

  if (!foundBeeYard) throw new BeeYardNotFound();

  const deletedBeeYard = await BeeYardModel.delete(beeYardId);

  if (!deletedBeeYard) throw new BeeYardDeleteError();
};

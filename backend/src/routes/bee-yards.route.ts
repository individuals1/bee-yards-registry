import express from "express";

import beeYardsController from "@controllers/bee-yards.controller";
import wrapperMiddleware from "@middleware/wrapper.middleware";

const router = express.Router();

const PREFIX = "/bee-yards";

router.get(
  PREFIX,
  wrapperMiddleware({
    validators: beeYardsController.validators.search,
    controller: beeYardsController.search,
  })
);

router.get(
  `${PREFIX}/:bee_yard_id`,
  wrapperMiddleware({
    validators: beeYardsController.validators.findSingle,
    controller: beeYardsController.findSingle,
  })
);

router.post(
  PREFIX,
  wrapperMiddleware({
    validators: beeYardsController.validators.create,
    controller: beeYardsController.create,
  })
);

router.put(
  `${PREFIX}/:bee_yard_id`,
  wrapperMiddleware({
    validators: beeYardsController.validators.update,
    controller: beeYardsController.update,
  })
);

router.delete(
  `${PREFIX}/:bee_yard_id`,
  wrapperMiddleware({
    validators: beeYardsController.validators.delete,
    controller: beeYardsController.delete,
  })
);

export default router;

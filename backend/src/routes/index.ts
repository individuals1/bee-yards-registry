import beeYardsRegistry from "./bee-yards.route";

const APP_PREFIX = "/api";

export default (app) => {
  app.use((req, res, next) => {
    next();
  });

  app.use(APP_PREFIX, beeYardsRegistry);
};

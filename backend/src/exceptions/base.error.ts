export default class BaseError extends Error {
  httpCode: number;

  constructor(message: string, serviceName = "General", httpCode = 500) {
    super(message);
    this.name = `${serviceName}.Error.${this.constructor.name}`;
    this.httpCode = httpCode;
  }
}

import BasicException from "./basic.exception";

export default class NotFoundException extends BasicException {
  constructor(statusInfo: string, name: string, statusDetail: string = null) {
    super(404, statusInfo, name, statusDetail);
  }
}

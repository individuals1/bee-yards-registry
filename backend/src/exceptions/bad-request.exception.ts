/** @format */

import BasicException from "./basic.exception";

export default class BadRequestException extends BasicException {
  constructor(statusInfo: string, statusDetail: string = null, name: string) {
    super(400, statusInfo, name, statusDetail);
  }
}

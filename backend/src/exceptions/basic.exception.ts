export default class BasicException extends Error {
  statusCode: number | string;
  statusDetail: string;
  statusInfo: string;

  constructor(
    statusCode: number | string,
    statusInfo: string,
    name: string,
    statusDetail: string = null
  ) {
    super(statusInfo || "Internal server error");

    Error.captureStackTrace(this, this.constructor);
    this.statusCode = statusCode || 500;
    this.statusDetail = statusDetail;
    this.statusInfo = statusInfo || "Internal server error";
    this.name = name;
  }
}

/** @format */

import BadRequestException from "./bad-request.exception";

export default class ValidatorException extends BadRequestException {
  constructor(result: any) {
    super(
      "Request validation failed",
      result
        .formatWith((err) => {
          return err.msg;
        })
        .mapped(),
      "General.Error.ValidationError"
    );
  }
}

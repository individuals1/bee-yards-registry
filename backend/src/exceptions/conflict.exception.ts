import BasicException from "./basic.exception";

export default class ConflictException extends BasicException {
  constructor(statusInfo: string, name: string, statusDetail: string = null) {
    super(409, statusInfo, name, statusDetail);
  }
}

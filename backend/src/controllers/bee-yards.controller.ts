/** @format */
import { Request, Response } from "express";
import { body, param, query } from "express-validator";
import { parseMatchedData } from "@helpers/functions.helper";

import NotFoundException from "@exceptions/not-found.exception";

import {
  createBeeYard,
  deleteBeeYard,
  findBeeYard,
  searchBeeYards,
  updateBeeYard,
} from "@services/bee-yards";
import { BeeYardInput } from "@interfaces/bee-yard.interface";
import ConflictException from "@exceptions/conflict.exception";

export default {
  validators: {
    search: [
      query("start_date").isISO8601().optional(),
      query("end_date").isISO8601().optional(),
    ],
    findSingle: [param("bee_yard_id").isNumeric()],
    create: [
      body("name").isLength({ min: 1, max: 255 }),
      body("date").isISO8601(),
      body("serial_number")
        .isLength({ min: 5, max: 5 })
        .matches(/^[0-9]+$/)
        .optional(),
    ],
    update: [
      param("bee_yard_id").isNumeric(),
      body("name").isLength({ min: 1, max: 255 }),
    ],
    delete: [param("bee_yard_id").isNumeric()],
  },

  search: async (req, res: Response): Promise<void> => {
    const payload = await searchBeeYards(
      req.query.start_date || null,
      req.query.end_date || null
    );

    res.status(200).json(payload);
  },

  findSingle: async (req: Request, res: Response): Promise<void> => {
    try {
      const payload = await findBeeYard(parseInt(req.params.bee_yard_id));

      res.status(200).send(payload);
    } catch (err) {
      switch (err.name) {
        case "BeeYardsService.Error.BeeYardNotFound":
          throw new NotFoundException("Bee yard was not found", err.name);

        default:
          throw err;
      }
    }
  },

  create: async (req: Request, res: Response): Promise<void> => {
    try {
      const beeYardInput = parseMatchedData(req) as BeeYardInput;
      const payload = await createBeeYard(beeYardInput);

      res.status(200).json(payload);
    } catch (err) {
      switch (err.name) {
        case "BeeYardsService.Error.RegistryNumberAlreadyExists":
          throw new ConflictException(
            "Bee yard registry number already exists",
            err.name
          );

        default:
          throw err;
      }
    }
  },

  update: async (req: Request, res: Response): Promise<void> => {
    try {
      const beeYardInput = parseMatchedData(req) as BeeYardInput;
      const payload = await updateBeeYard(
        parseInt(req.params.bee_yard_id),
        beeYardInput
      );

      res.status(200).json(payload);
    } catch (err) {
      switch (err.name) {
        case "BeeYardsService.Error.BeeYardNotFound":
          throw new NotFoundException("Bee yard was not found", err.name);

        default:
          throw err;
      }
    }
  },

  delete: async (req: Request, res: Response): Promise<void> => {
    try {
      await deleteBeeYard(parseInt(req.params.bee_yard_id));

      res.status(204).send();
    } catch (err) {
      switch (err.name) {
        case "BeeYardsService.Error.BeeYardNotFound":
          throw new NotFoundException("Bee yard was not found", err.name);

        default:
          throw err;
      }
    }
  },
};

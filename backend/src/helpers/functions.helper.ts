import { Request } from "express";
import { matchedData } from "express-validator";
import camelCase from "camelcase";

export const toCamelCase = (element: object | unknown[]) => {
  if (element instanceof Array) {
    return element.map((value) => {
      if (typeof value === "object") {
        value = toCamelCase(value);
      }

      return value;
    });
  }

  let newObject = {};
  let value = null;

  for (const key in element) {
    value = element[key];

    if ((value && value.constructor === Object) || value instanceof Array) {
      value = toCamelCase(value);
    }

    newObject[camelCase(key)] = value;
  }

  return newObject;
};

export const parseMatchedData = (req: Request) => {
  if (!req) return null;

  return toCamelCase(
    matchedData(req, { locations: ["body"], includeOptionals: true })
  );
};

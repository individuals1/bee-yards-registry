import config from "config";
import Knex from "knex";
import decamelize from "decamelize";
import { toCamelCase } from "./functions.helper";

export const initKnex = (host: string, port: string) => {
  return require("knex")({
    client: "pg",
    connection: {
      host,
      user: config.get("database.user"),
      password: config.get("database.password"),
      database: config.get("database.databaseName"),
      port,
    },
    wrapIdentifier: (value, origImpl) => origImpl(decamelize(value)),
    postProcessResponse: (result, _queryContext) => {
      if (!result) return;

      if (Array.isArray(result)) return result.map(toCamelCase);

      return toCamelCase(result);
    },
    migrations: {
      tableName: "migrations",
    },
  });
};

(Knex as any).QueryBuilder.extend("getFirst", async function () {
  const result = await this;

  if (result && result?.length) return result[0];

  return null;
});

export const knex = initKnex(
  config.get("database.host"),
  config.get("database.port")
);

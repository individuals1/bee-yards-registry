/** @format */

export default (err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }
  if (err.statusInfo === undefined) {
    err.statusCode = 422;
    err.statusInfo = err.message;
    err.statusDetail = err.detail;
    err.name = "General.Error.InternalError";
  }

  res.status(err.statusCode).send({
    status: "error",
    message: err.statusInfo,
    details: err.statusDetail,
    name: err.name,
    statusCode: err.statusCode,
  });
};

import Wrapper from "@interfaces/middleware.interface";
import { Request, Response, NextFunction } from "express";
import validate from "@middleware/validation.middleware";
import { ValidationChain } from "express-validator";

export default (wrapperObject: Wrapper) => {
  return async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    const validators = wrapperObject?.validators as ValidationChain[];

    const controller = wrapperObject?.controller;

    validate(validators, req, res, async (validationError) => {
      if (validationError) return next(validationError);

      try {
        await controller(req, res);
        return next();
      } catch (err) {
        return next(err);
      }
    });
  };
};

import { Request, Response, NextFunction } from "express";

import ValidatorException from "@exceptions/validator.exception";
import { ValidationChain, validationResult } from "express-validator";

export default async (
  validations: ValidationChain[],
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  await Promise.all(validations.map((validation) => validation.run(req)));

  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  return next(new ValidatorException(errors));
};

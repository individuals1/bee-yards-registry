/**
 * @format
 * @type {import('ts-jest/dist/types').InitialOptionsTsJest}
 */

module.exports = {
  modulePaths: ["<rootDir>", "./src"],
  moduleNameMapper: {
    "@/(.*)": "<rootDir>/$1",
    "src/(.*)": "<rootDir>/src/$1",
    "mocks/(.*)": "<rootDir>/tests/mocks/$1",
    "@controllers/(.*)": "<rootDir>/src/controllers/$1",
    "@enums/(.*)": "<rootDir>/src/enums/$1",
    "@exceptions/(.*)": "<rootDir>/src/exceptions/$1",
    "@helpers/(.*)": "<rootDir>/src/helpers/$1",
    "@middleware/(.*)": "<rootDir>/src/middleware/$1",
    "@models/(.*)": "<rootDir>/src/models/$1",
    "@routes/(.*)": "<rootDir>/src/routes/$1",
    "@services/(.*)": "<rootDir>/src/services/$1",
  },
  moduleFileExtensions: ["ts", "js", "json"],
  preset: "ts-jest",
  transform: {
    "node_modules/variables/.+\\.(j|t)sx?$": "ts-jest",
  },
  collectCoverage: true,
  collectCoverageFrom: [
    "**/*.ts",
    "!**/services/**/index.ts",
    "!**/tests/**",
    "!**/interfaces/**",
    "!**/migrations/**",
    "!*",
  ],
  coveragePathIgnorePatterns: [
    "<rootDir>/config/",
    "<rootDir>/node_modules/",
    "<rootDir>/dist/",
    "<rootDir>/src/controllers/",
    "<rootDir>/src/enums/",
    "<rootDir>/src/exceptions/",
    "<rootDir>/src/helpers/",
    "<rootDir>/src/middleware/",
    "<rootDir>/src/models/",
    "<rootDir>/src/routes/",
  ],
  transformIgnorePatterns: ["node_modules/(?!variables/.*)", "dist/*"],
  coverageThreshold: {
    "./src/services/": {
      branches: 70,
      functions: 70,
      lines: 70,
      statements: 70,
    },
  },
  globals: {
    "ts-jest": {
      isolatedModules: true,
    },
  },
  clearMocks: true,
};

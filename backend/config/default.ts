export default {
  server: {
    port: 8001,
  },
  database: {
    host: "localhost",
    port: "5432",
    databaseName: "bee_yards_registry_local",
    user: "postgres",
    password: "password",
  },
};

export const beeYardInput = {
  name: "Pasieka nr.1",
  date: "2022-06-09",
  serialNumber: "00002",
};

export const beeYardData = {
  id: 1,
  name: "Pasieka nr.1",
  date: "2022-06-09",
  registerNumber: "2022060900002708",
};

export const beeYardsData = [
  {
    id: 2,
    name: "Pasieka nr.0",
    date: "2022-06-08",
    registerNumber: "2022060900002708",
  },
  {
    id: 1,
    name: "Pasieka nr.1",
    date: "2022-06-09",
    registerNumber: "2022060900002708",
  },
  {
    id: 3,
    name: "Pasieka nr.2",
    date: "2022-06-10",
    registerNumber: "2022061000002708",
  },
  {
    id: 4,
    name: "Pasieka nr.3",
    date: "2022-06-11",
    registerNumber: "2022061100002708",
  },
  {
    id: 5,
    name: "Pasieka nr.4",
    date: "2022-06-12",
    registerNumber: "2022061200002708",
  },
];

export const startDateData = [
  {
    id: 1,
    name: "Pasieka nr.1",
    date: "2022-06-09",
    registerNumber: "2022060900002708",
  },
  {
    id: 3,
    name: "Pasieka nr.2",
    date: "2022-06-10",
    registerNumber: "2022061000002708",
  },
  {
    id: 4,
    name: "Pasieka nr.3",
    date: "2022-06-11",
    registerNumber: "2022061100002708",
  },
  {
    id: 5,
    name: "Pasieka nr.4",
    date: "2022-06-12",
    registerNumber: "2022061200002708",
  },
];

export const endDateData = [
  {
    id: 2,
    name: "Pasieka nr.0",
    date: "2022-06-08",
    registerNumber: "2022060900002708",
  },
  {
    id: 1,
    name: "Pasieka nr.1",
    date: "2022-06-09",
    registerNumber: "2022060900002708",
  },
  {
    id: 3,
    name: "Pasieka nr.2",
    date: "2022-06-10",
    registerNumber: "2022061000002708",
  },
];

export const bothDatesData = [
  {
    id: 1,
    name: "Pasieka nr.1",
    date: "2022-06-09",
    registerNumber: "2022060900002708",
  },
  {
    id: 3,
    name: "Pasieka nr.2",
    date: "2022-06-10",
    registerNumber: "2022061000002708",
  },
];

export const beeYardRegistersData = [
  {
    id: 1,
    name: "Pasieka nr.1",
    date: "2022-06-09",
    registerNumber: "2022060900002708",
  },
  {
    id: 2,
    name: "Pasieka nr.6",
    date: "2022-06-09",
    registerNumber: "2022060912982572",
  },
  {
    id: 3,
    name: "Pasieka nr.7",
    date: "2022-06-09",
    registerNumber: "2022060912222384",
  },
];

export const firstThreeSerialNumbersUsed = [
  {
    id: 1,
    name: "Pasieka nr.1",
    date: "2022-06-09",
    registerNumber: "2022060900001000",
  },
  {
    id: 2,
    name: "Pasieka nr.6",
    date: "2022-06-09",
    registerNumber: "2022060900002000",
  },
  {
    id: 3,
    name: "Pasieka nr.7",
    date: "2022-06-09",
    registerNumber: "2022060900003000",
  },
];

/** @format */

import { mocked } from "ts-jest/utils";

import BeeYardModel from "@models/bee-yard.model";
import createBeeYard from "@services/bee-yards/create";
import { BeeYardCreateError } from "@services/bee-yards/error";

import { generateRegisterNumber } from "@services/bee-yards/helper";

import { beeYardInput, beeYardData } from "@mocks/bee-yard.mock";

jest.mock("@models/bee-yard.model");
jest.mock("@services/bee-yards/helper");

const BeeYardModelMock = mocked(BeeYardModel, true);
const generateRegisterNumberMock = mocked(generateRegisterNumber, true);

const method = createBeeYard;
const registerNumber = "2022060900002708";

describe("BeeYardsService.createBeeYard", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should return new bee yard registry", async () => {
    generateRegisterNumberMock.mockResolvedValue(registerNumber);
    BeeYardModelMock.create.mockResolvedValue(beeYardData);

    const result = await method(beeYardInput);

    expect(generateRegisterNumberMock).toHaveBeenCalledTimes(1);
    expect(BeeYardModelMock.create).toHaveBeenCalled();
    expect(result).toBe(beeYardData);
  });

  test("should throw BeeYardCreateError if bee yard registry was not created", async () => {
    generateRegisterNumberMock.mockResolvedValue(registerNumber);
    BeeYardModelMock.create.mockResolvedValue(null);

    await expect(method(beeYardInput)).rejects.toBeInstanceOf(
      BeeYardCreateError
    );

    expect(generateRegisterNumberMock).toHaveBeenCalledTimes(1);
    expect(BeeYardModelMock.create).toHaveBeenCalled();
  });
});

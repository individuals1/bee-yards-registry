/** @format */

import { mocked } from "ts-jest/utils";

import BeeYardModel from "@models/bee-yard.model";
import searchBeeYards from "@services/bee-yards/search";

import {
  beeYardsData,
  startDateData,
  endDateData,
  bothDatesData,
} from "@mocks/bee-yard.mock";

jest.mock("@models/bee-yard.model");

const BeeYardModelMock = mocked(BeeYardModel, true);

const method = searchBeeYards;
const startDate = "2022-06-09";
const endDate = "2022-06-10";

describe("BeeYardsService.searchBeeYards", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("Should return all bee yards registry if no startDate and endDate provided", async () => {
    BeeYardModelMock.search.mockResolvedValue(beeYardsData);

    const result = await method(null, null);

    expect(BeeYardModelMock.search).toHaveBeenCalledWith(null, null);

    expect(result).toEqual(beeYardsData);
  });

  test("Should return filtered by startDate bee yards registry if startDate provided and no endDate provided", async () => {
    BeeYardModelMock.search.mockResolvedValue(startDateData);

    const result = await method(startDate, null);

    expect(BeeYardModelMock.search).toHaveBeenCalledWith(startDate, null);

    expect(result).toEqual(startDateData);
  });

  test("Should return filtered by endDate bee yards registry if endDate provided and no startDate provided", async () => {
    BeeYardModelMock.search.mockResolvedValue(endDateData);

    const result = await method(null, endDate);

    expect(BeeYardModelMock.search).toHaveBeenCalledWith(null, endDate);

    expect(result).toEqual(endDateData);
  });

  test("Should return filtered by startDate and endDate bee yards registry if both startDate and endDate provided", async () => {
    BeeYardModelMock.search.mockResolvedValue(bothDatesData);

    const result = await method(startDate, endDate);

    expect(BeeYardModelMock.search).toHaveBeenCalledWith(startDate, endDate);

    expect(result).toEqual(bothDatesData);
  });

  test("Should return empty array if no data found", async () => {
    BeeYardModelMock.search.mockResolvedValue([]);

    const result = await method(null, null);

    expect(BeeYardModelMock.search).toHaveBeenCalledWith(null, null);
    expect(result).toEqual([]);
  });
});

/** @format */

import { mocked } from "ts-jest/utils";

import BeeYardModel from "@models/bee-yard.model";
import deleteBeeYard from "@services/bee-yards/delete";
import { BeeYardNotFound, BeeYardDeleteError } from "@services/bee-yards/error";

import { beeYardData } from "@mocks/bee-yard.mock";

jest.mock("@models/bee-yard.model");

const BeeYardModelMock = mocked(BeeYardModel, true);

const method = deleteBeeYard;
const beeYardId = 1;

describe("BeeYardsService.deleteBeeYard", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should delete bee yard registry and return undefined", async () => {
    BeeYardModelMock.find.mockResolvedValue(beeYardData);
    BeeYardModelMock.delete.mockResolvedValue(beeYardData);

    const result = await method(beeYardId);

    expect(BeeYardModelMock.find).toHaveBeenCalledWith(beeYardId);
    expect(BeeYardModelMock.delete).toHaveBeenCalledWith(beeYardId);
    expect(result).toBe(undefined);
  });

  test("should throw BeeYardNotFound if bee yard registry was not found", async () => {
    BeeYardModelMock.find.mockResolvedValue(null);

    await expect(method(beeYardId)).rejects.toBeInstanceOf(BeeYardNotFound);

    expect(BeeYardModelMock.find).toHaveBeenCalledWith(beeYardId);
    expect(BeeYardModelMock.delete).not.toHaveBeenCalled();
  });

  test("should throw BeeYardDeleteError if bee yard registry was not deleted", async () => {
    BeeYardModelMock.find.mockResolvedValue(beeYardData);
    BeeYardModelMock.delete.mockResolvedValue(null);

    await expect(method(beeYardId)).rejects.toBeInstanceOf(BeeYardDeleteError);

    expect(BeeYardModelMock.find).toHaveBeenCalledWith(beeYardId);
    expect(BeeYardModelMock.delete).toHaveBeenCalled();
  });
});

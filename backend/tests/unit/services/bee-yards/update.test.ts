/** @format */

import { mocked } from "ts-jest/utils";

import BeeYardModel from "@models/bee-yard.model";
import updateBeeYard from "@services/bee-yards/update";
import { BeeYardNotFound, BeeYardUpdateError } from "@services/bee-yards/error";

import { beeYardData } from "@mocks/bee-yard.mock";

jest.mock("@models/bee-yard.model");

const BeeYardModelMock = mocked(BeeYardModel, true);

const method = updateBeeYard;
const beeYardId = 1;

const beeYardInput = {
  name: "Pasieka nr.2",
};

describe("BeeYardsService.updateBeeYard", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should return updated bee yard registry", async () => {
    BeeYardModelMock.find.mockResolvedValue(beeYardData);
    BeeYardModelMock.update.mockResolvedValue({
      ...beeYardData,
      name: beeYardInput.name,
    });

    const result = await method(beeYardId, beeYardInput);

    expect(BeeYardModelMock.find).toHaveBeenCalledWith(beeYardId);
    expect(BeeYardModelMock.update).toHaveBeenCalledWith(
      beeYardId,
      beeYardInput
    );
    expect(result).toStrictEqual({ ...beeYardData, name: beeYardInput.name });
  });

  test("should throw BeeYardNotFound if bee yard registry was not found", async () => {
    BeeYardModelMock.find.mockResolvedValue(null);

    await expect(method(beeYardId, beeYardInput)).rejects.toBeInstanceOf(
      BeeYardNotFound
    );

    expect(BeeYardModelMock.find).toHaveBeenCalledWith(beeYardId);
    expect(BeeYardModelMock.update).not.toHaveBeenCalled();
  });

  test("should throw BeeYardUpdateError if bee yard registry was not updated", async () => {
    BeeYardModelMock.find.mockResolvedValue(beeYardData);
    BeeYardModelMock.update.mockResolvedValue(null);

    await expect(method(beeYardId, beeYardInput)).rejects.toBeInstanceOf(
      BeeYardUpdateError
    );

    expect(BeeYardModelMock.find).toHaveBeenCalledWith(beeYardId);
    expect(BeeYardModelMock.update).toHaveBeenCalledWith(
      beeYardId,
      beeYardInput
    );
  });
});

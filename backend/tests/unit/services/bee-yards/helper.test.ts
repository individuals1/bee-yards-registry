/** @format */

import { mocked } from "ts-jest/utils";

import BeeYardModel from "@models/bee-yard.model";
import {
  generateRegisterNumber,
  functionsToTest,
} from "@services/bee-yards/helper";
import {
  RegistryNumberAlreadyExists,
  ExceedsAllowedSerialNumber,
} from "@services/bee-yards/error";

const {
  checkSerialNumber,
  generateSerialNumber,
  getAllDailySerialNumbers,
  calculateChecksum,
  parseDate,
} = functionsToTest;

import {
  beeYardData,
  beeYardsData,
  beeYardRegistersData,
  firstThreeSerialNumbersUsed,
} from "@mocks/bee-yard.mock";

jest.mock("@models/bee-yard.model");

const BeeYardModelMock = mocked(BeeYardModel, true);

const method = generateRegisterNumber;
const date = "2022-06-09";
const optionalSerialNumber = "00002";
const firstOfDayRegisterNumber = "2022060900001732";
const registerNumberWithOptionalSerialNumber = "2022060900002708";
const registerNumberWithUniqueSerialNumber = "2022060912345060";
const allDailySerialNumbers = ["00002", "12982", "12222"];

describe("BeeYardsService.helper", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe("generateRegisterNumber function", () => {
    test("Should return register number with calculated checksum / no optionalSerialNumber provided, bee yard registers found", async () => {
      BeeYardModelMock.findAllByDate.mockResolvedValue([beeYardData]);

      const result = await method(date, null);

      generateSerialNumber([beeYardData]);

      expect(BeeYardModelMock.findAllByDate).toHaveBeenCalledWith(date);
      expect(result).toEqual(firstOfDayRegisterNumber);
    });

    test("Should return register number with calculated checksum / no optionalSerialNumber provided, no bee yard registers found", async () => {
      BeeYardModelMock.findAllByDate.mockResolvedValue([]);

      const result = await method(date, null);

      expect(BeeYardModelMock.findAllByDate).toHaveBeenCalledWith(date);
      expect(result).toEqual(firstOfDayRegisterNumber);
    });

    test("Should return register number with calculated checksum / optionalSerialNumber provided", async () => {
      const uniqueSerialNumber = "12345";
      BeeYardModelMock.findAllByDate.mockResolvedValue(beeYardsData);

      const result = await method(date, uniqueSerialNumber);

      checkSerialNumber(beeYardsData, uniqueSerialNumber);

      expect(BeeYardModelMock.findAllByDate).toHaveBeenCalledWith(date);
      expect(result).toEqual(registerNumberWithUniqueSerialNumber);
    });

    test("Should return register number with calculated checksum / optionalSerialNumber provided but no beeYardsFound", async () => {
      BeeYardModelMock.findAllByDate.mockResolvedValue([]);

      const result = await method(date, optionalSerialNumber);

      expect(BeeYardModelMock.findAllByDate).toHaveBeenCalledWith(date);

      expect(result).toEqual(registerNumberWithOptionalSerialNumber);
    });
  });

  describe("parseDate function", () => {
    test("should return string of date in format YYYYMMDD", async () => {
      const result = await parseDate(date);

      expect(result).toEqual("20220609");
    });

    test("should return null if date is not in valid format (does not include '-' character)", async () => {
      const wrongDate = "2022/06/09";

      const result = await parseDate(wrongDate);

      expect(result).toEqual(null);
    });
  });

  describe("calculateChecksum function", () => {
    test("should return checksum for provided registerNumber '2022060900001'", () => {
      const registerNumber = "2022060900001";
      const checksum = "732";

      const result = calculateChecksum(registerNumber);

      expect(result).toEqual(checksum);
    });

    test("should return checksum for provided registerNumber '2022060999999'", () => {
      const registerNumber = "2022060999999";
      const checksum = "192";

      const result = calculateChecksum(registerNumber);

      expect(result).toEqual(checksum);
    });
  });

  describe("getAllDailySerialNumbers function", () => {
    test("should return isolated serial number out of all catched daily serial numbers", async () => {
      const result = await getAllDailySerialNumbers(beeYardRegistersData);

      expect(result).toEqual(allDailySerialNumbers);
    });
  });

  describe("generateSerialNumber function", () => {
    test("should return first daily serial number if no registry with it exists", () => {
      const firstDailySerialNumber = "00001";

      const result = generateSerialNumber(beeYardRegistersData);

      expect(result).toEqual(firstDailySerialNumber);
    });

    test("should return next iterated possible daily serial number if registry with serial numbers exists", () => {
      const nextIteratedSerialNumber = "00004";

      const result = generateSerialNumber(firstThreeSerialNumbersUsed);

      expect(result).toEqual(nextIteratedSerialNumber);
    });

    test("should throw ExceedsAllowedSerialNumber if serial number exceeds all allowed daily serial numbers (99999)", () => {
      const exceededDailyRegistersData = [];
      let serialNumber = "00001";

      //push first one
      exceededDailyRegistersData.push({
        name: "Pasieka test",
        date: "2022-06-09",
        registerNumber: `20220609${serialNumber}123`,
      });

      for (let i = 0; i < 99999; i++) {
        serialNumber = (parseInt(serialNumber) + 1).toString().padStart(5, "0");

        exceededDailyRegistersData.push({
          name: "Pasieka test",
          date: "2022-06-09",
          registerNumber: `20220609${serialNumber}123`,
        });
      }

      expect(() => {
        generateSerialNumber(exceededDailyRegistersData);
      }).toThrow(ExceedsAllowedSerialNumber);
    });
  });

  describe("checkSerialNumber function", () => {
    test("should return undefined if provided serial number is unique for a provided date", () => {
      const uniqueSerialNumber = "00010";

      const result = checkSerialNumber(
        beeYardRegistersData,
        uniqueSerialNumber
      );

      expect(result).toEqual(undefined);
    });

    test("should throw RegistryNumberAlreadyExists if provided serial number already exists for a provided date", () => {
      expect(() => {
        checkSerialNumber(beeYardRegistersData, optionalSerialNumber);
      }).toThrow(RegistryNumberAlreadyExists);
    });
  });
});

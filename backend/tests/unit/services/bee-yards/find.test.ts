/** @format */

import { mocked } from "ts-jest/utils";

import BeeYardModel from "@models/bee-yard.model";
import findBeeYard from "@services/bee-yards/find";
import { BeeYardNotFound } from "@services/bee-yards/error";

import { beeYardData } from "@mocks/bee-yard.mock";

jest.mock("@models/bee-yard.model");

const BeeYardModelMock = mocked(BeeYardModel, true);

const method = findBeeYard;

const beeYardId = 1;

describe("BeeYardsService.findBeeYard", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should return found bee yard registry", async () => {
    BeeYardModelMock.find.mockResolvedValue(beeYardData);

    const result = await method(beeYardId);

    expect(BeeYardModelMock.find).toHaveBeenCalledWith(beeYardId);
    expect(result).toBe(beeYardData);
  });

  test("should throw BeeYardNotFound if bee yard registry was not found", async () => {
    BeeYardModelMock.find.mockResolvedValue(null);

    await expect(method(beeYardId)).rejects.toBeInstanceOf(BeeYardNotFound);

    expect(BeeYardModelMock.find).toHaveBeenCalledWith(beeYardId);
  });
});

import config from "config";

export default {
  development: {
    client: "postgres",
    connection: {
      host: config.get("database.host"),
      port: config.get("database.port"),
      database: config.get("database.databaseName"),
      user: config.get("database.user"),
      password: config.get("database.password"),
    },
    migrations: {
      tableName: "migrations",
      directory: "./migrations",
    },
  },
};

import "module-alias/register";
import config from "config";
import express from "express";
import bodyParser from "body-parser";
import routes from "@routes/index";
import errorHandlerMiddleware from "@middleware/error-handler.middleware";

import swaggerDocs from "@docs/swagger";

const app = express();
const port: number = config.get("server.port");

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header(
    "Access-Control-Allow-Methods",
    "GET,HEAD,OPTIONS,POST,PUT,DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, X-Authorization"
  );
  next();
});

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

routes(app);
app.use(errorHandlerMiddleware);

app.listen(port, () => {
  console.log(`Bee Yards Registry is listening on port ${port}!`);

  swaggerDocs(app, port);
});

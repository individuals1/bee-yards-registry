import { Router } from "next/router";
import React from "react";
import EditRegistry from "../../../components/EditRegistry/EditRegistry";
import Layout from "../../../components/Layout";
import NotFound from "../../404";

const EditBeeYardRegistry = (props) => {
  const { beeYardData, error } = props;
  if (error) return <NotFound message={error} />;
  return <Layout component={<EditRegistry beeYardData={beeYardData} />} />;
};

EditBeeYardRegistry.getInitialProps = async (ctx) => {
  const response = await fetch(
    `http://localhost:8001/api/bee-yards/${ctx.query?.beeYardId}`,
    {
      method: "GET",
    }
  );
  const beeYardData = await response.json();

  const error = beeYardData?.status === "error" ? beeYardData?.message : false;

  return { beeYardData, error };
};

export default EditBeeYardRegistry;

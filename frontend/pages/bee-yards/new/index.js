import React from "react";
import AddNewRegistry from "../../../components/AddNewRegistry/AddNewRegistry";
import Layout from "../../../components/Layout";

const NewRegistry = () => {
  return <Layout component={<AddNewRegistry />} />;
};

export default NewRegistry;

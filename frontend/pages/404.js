import Link from "next/link";

import ErrorStyles from "../styles/Error.module.css";

const NotFound = ({ message }) => {
  return (
    <div className={ErrorStyles.wrapper}>
      <h1 className={ErrorStyles.title}>{message}</h1>
      <p className={ErrorStyles.description}>
        <Link href="/">Go back</Link>
      </p>
    </div>
  );
};

export default NotFound;

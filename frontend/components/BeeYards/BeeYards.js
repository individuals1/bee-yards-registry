import DataTable from "react-data-table-component";

import React, { useState } from "react";
import useBeeYards from "./BeeYards.hook";
import BeeYardsStyles from "./BeeYards.module.scss";
import AddButton from "../Buttons/AddButton/AddButton";
import DatePickerComponent from "../DatePicker/DatePicker";

const BeeYards = () => {
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const { columns, data } = useBeeYards(startDate, endDate);

  const getFirstDate = (firstDate) => {
    setStartDate(firstDate);
  };

  const getLastDate = (lastDate) => {
    setEndDate(lastDate);
  };

  return (
    <div className={BeeYardsStyles.wrapper}>
      <div className={`${BeeYardsStyles.beeGrid} ${BeeYardsStyles.beeImage}`} />
      <div className={BeeYardsStyles.titleGrid}>
        <div className={`${BeeYardsStyles.title}`}>Bee Yards Registry</div>
      </div>

      <div className={BeeYardsStyles.buttonGrid}>
        <div className={BeeYardsStyles.flex}>
          <AddButton />
        </div>
      </div>

      <div className={BeeYardsStyles.datePickerGrid}>
        <div className={BeeYardsStyles.datePicker}>
          <DatePickerComponent
            firstDate={getFirstDate}
            lastDate={getLastDate}
          />
        </div>
      </div>

      <div className={BeeYardsStyles.dataTableGrid}>
        <div className={BeeYardsStyles.container}>
          <DataTable columns={columns} data={data?.beeYards} />
        </div>
      </div>
    </div>
  );
};

export default BeeYards;

import moment from "moment-timezone";
import React, { useEffect, useState } from "react";
import EditButton from "../Buttons/EditButton/EditButton";

const mapElement = (element) => {
  return {
    id: element.id,
    name: element.name,
    date: moment(element.date).format("L"),
    registerNumber: element.registerNumber,
  };
};

const mapFunction = (response) => {
  if (response) {
    const data = response?.map(mapElement);

    return { beeYards: data };
  } else {
    return null;
  }
};

const buildPath = (startDate, endDate) => {
  let url = "http://localhost:8001/api/bee-yards";

  if (startDate && !endDate) url += `?start_date=${startDate}`;
  if (!startDate && endDate) url += `?end_date=${endDate}`;
  if (startDate && endDate)
    url += `?start_date=${startDate}&end_date=${endDate}`;

  return url;
};

const useBeeYards = (startDate, endDate) => {
  const [data, setData] = useState(null);
  const [isCalled, setIsCalled] = useState(false);

  const getData = async () => {
    const response = await fetch(buildPath(startDate, endDate), {
      method: "GET",
    });
    const data = await response.json();
    setData(data);
  };

  useEffect(() => {
    if (data && isCalled) return;

    getData();
    setIsCalled(true);
  });

  useEffect(() => {
    if (!startDate && !endDate) return;

    setIsCalled(false);
  }, [startDate, endDate]);

  let parsedData;
  if (data) {
    parsedData = mapFunction(data);
  }

  const columns = [
    {
      name: "Name",
      id: "name",
      selector: (row) => row.name,
    },
    {
      name: "Date",
      id: "date",
      selector: (row) => row.date,
    },
    {
      name: "Register Number",
      id: "registerNumber",
      selector: (row) => row.registerNumber,
      sortable: true,
    },
    {
      name: "",
      id: "edit",
      selector: (row) => row["preview"],
      sortable: false,
      search: false,
      cell: (row) => <EditButton row={row} />,
      width: "100px",
      center: true,
    },
  ];

  return { columns, data: parsedData };
};

export default useBeeYards;

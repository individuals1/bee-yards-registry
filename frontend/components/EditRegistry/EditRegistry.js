import React, { useState } from "react";
import moment from "moment-timezone";

import { useForm } from "react-hook-form";
import Button from "../Buttons/Button";

import EditRegistryStyles from "./EditRegistry.module.scss";
import { useRouter } from "next/router";

const EditRegistry = (props) => {
  const { beeYardData } = props;

  const router = useRouter();
  const beeYardId = router?.query?.beeYardId;

  const [success, setSuccess] = useState(false);
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues: {
      name: beeYardData.name,
      date: moment(beeYardData.date).format("YYYY-MM-DD"),
      registerNumber: beeYardData?.registerNumber,
    },
  });

  const callData = async (data) => {
    const response = await fetch(
      `http://localhost:8001/api/bee-yards/${beeYardId}`,
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: data.name,
        }),
      }
    );

    if (response) setSuccess(true);
  };

  const onSubmit = (data) => {
    callData(data);
  };

  return (
    <div className={EditRegistryStyles.wrapper}>
      <div
        className={`${EditRegistryStyles.beeGrid} ${EditRegistryStyles.beeImage}`}
      />
      <div className={EditRegistryStyles.formGrid}>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className={EditRegistryStyles.form}
        >
          <div
            className={`${EditRegistryStyles.titleGrid} ${EditRegistryStyles.title}`}
          >
            Bee Yard Registry
          </div>
          <div
            className={`${EditRegistryStyles.subTitleGrid} ${EditRegistryStyles.subTitle}`}
          >
            Edit existing registry
          </div>

          <div className={`${EditRegistryStyles.nameGrid}`}>
            <input
              className={`${EditRegistryStyles.name}`}
              {...register("name", { required: "This field is required" })}
              placeholder="Name"
            />
          </div>
          <p className={`${EditRegistryStyles.nameErrorGrid}`}>
            {errors.name?.message}
          </p>

          <div className={`${EditRegistryStyles.dateGrid}`}>
            <input
              readOnly
              type="date"
              className={`${EditRegistryStyles.date}`}
              {...register("date")}
              placeholder="Date"
            />
          </div>

          <div className={`${EditRegistryStyles.serialNumberGrid}`}>
            <input
              readOnly
              className={`${EditRegistryStyles.serialNumber}`}
              {...register("registerNumber")}
              placeholder="Register Number"
            />
          </div>

          <div className={`${EditRegistryStyles.submitGrid}`}>
            <input
              type="submit"
              value="Submit"
              className={`${EditRegistryStyles.submit}`}
            />
          </div>

          <div className={`${EditRegistryStyles.goBackGrid}`}>
            <Button description={"Go back"} link={`/`} style={"primary"} />
          </div>

          <div className={`${EditRegistryStyles.deleteGrid}`}>
            <Button description={"Delete"} style={"delete"} id={beeYardId} />
          </div>

          {success && (
            <div className={`${EditRegistryStyles.successGrid}`}>
              <Button description={"Success!"} style={"success"} />
            </div>
          )}
        </form>
      </div>
    </div>
  );
};

export default EditRegistry;

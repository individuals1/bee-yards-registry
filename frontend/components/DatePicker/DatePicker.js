import { forwardRef, useEffect, useState } from "react";
import DatePicker from "react-datepicker";
import moment from "moment-timezone";
import DatePickerStyles from "./DatePicker.module.scss";
import "react-datepicker/dist/react-datepicker.css";
import Button from "../Buttons/Button";

const DatePickerComponent = ({ firstDate, lastDate }) => {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  useEffect(() => {
    if (!startDate) return;

    firstDate(moment(startDate).format("YYYY-MM-DD"));
  }, [startDate]);

  useEffect(() => {
    if (!endDate) return;

    lastDate(moment(endDate).format("YYYY-MM-DD"));
  }, [endDate]);

  const Input = forwardRef(({ value, onClick, text }, ref) => (
    <button className={DatePickerStyles.wrapper} onClick={onClick} ref={ref}>
      {value ? value : text}
    </button>
  ));

  return (
    <>
      <DatePicker
        selected={startDate}
        onChange={(date) => setStartDate(date)}
        selectsStart
        startDate={startDate}
        endDate={endDate}
        customInput={<Input text="Start Date" />}
      />

      <DatePicker
        selected={endDate}
        onChange={(date) => setEndDate(date)}
        selectsEnd
        startDate={startDate}
        endDate={endDate}
        minDate={startDate}
        customInput={<Input text="End Date" />}
      />
    </>
  );
};

export default DatePickerComponent;

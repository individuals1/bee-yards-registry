import React, { useState } from "react";

import { useForm } from "react-hook-form";
import Button from "../Buttons/Button";

import AddNewRegistryStyles from "./AddNewRegistry.module.scss";

const AddNewRegistry = () => {
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const callData = async (data) => {
    const response = await fetch("http://localhost:8001/api/bee-yards", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: data.name,
        date: data.date,
        serial_number: data.serialNumber,
      }),
    });

    const errorCode = response.ok ? false : response.status;

    if (errorCode === 409) {
      setError("Bee yard registry number already exists!");
    }

    if (!errorCode) setSuccess(true);
  };

  const onSubmit = (data) => {
    callData(data);
  };

  return (
    <div className={AddNewRegistryStyles.wrapper}>
      <div
        className={`${AddNewRegistryStyles.beeGrid} ${AddNewRegistryStyles.beeImage}`}
      />
      <div className={AddNewRegistryStyles.formGrid}>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className={AddNewRegistryStyles.form}
        >
          <div
            className={`${AddNewRegistryStyles.titleGrid} ${AddNewRegistryStyles.title}`}
          >
            Bee Yard Registry
          </div>
          <div
            className={`${AddNewRegistryStyles.subTitleGrid} ${AddNewRegistryStyles.subTitle}`}
          >
            Add new one!
          </div>

          <div className={`${AddNewRegistryStyles.nameGrid}`}>
            <input
              className={`${AddNewRegistryStyles.name}`}
              {...register("name", { required: "This field is required" })}
              placeholder="Name"
            />
          </div>
          <p className={`${AddNewRegistryStyles.nameErrorGrid}`}>
            {errors.name?.message}
          </p>

          <div className={`${AddNewRegistryStyles.dateGrid}`}>
            <input
              type="date"
              className={`${AddNewRegistryStyles.date}`}
              {...register("date", { required: "This field is required" })}
              placeholder="Date"
            />
          </div>
          <p className={`${AddNewRegistryStyles.dateErrorGrid}`}>
            {errors.date?.message}
          </p>

          <div className={`${AddNewRegistryStyles.serialNumberGrid}`}>
            <input
              className={`${AddNewRegistryStyles.serialNumber}`}
              {...register("serialNumber", {
                minLength: {
                  value: 5,
                  message: "Length cannot be less than 5 numbers",
                },
                maxLength: {
                  value: 5,
                  message: "Length cannot be greater than 5 numbers",
                },
              })}
              placeholder="Serial Number"
            />
          </div>

          <p className={`${AddNewRegistryStyles.serialNumberErrorGrid}`}>
            {errors.serialNumber?.message}
          </p>

          <div className={`${AddNewRegistryStyles.submitGrid}`}>
            <input
              type="submit"
              value="Submit"
              className={`${AddNewRegistryStyles.submit}`}
            />
          </div>

          <div className={`${AddNewRegistryStyles.goBackGrid}`}>
            <Button description={"Go back"} link={`/`} style={"primary"} />
          </div>

          {success && (
            <div className={`${AddNewRegistryStyles.successGrid}`}>
              <Button description={"Success!"} style={"success"} />
            </div>
          )}

          {error && (
            <div className={`${AddNewRegistryStyles.successGrid}`}>
              <Button description={`${error}`} style={"error"} />
            </div>
          )}
        </form>
      </div>
    </div>
  );
};

export default AddNewRegistry;

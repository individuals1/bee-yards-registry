import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";

import ButtonStyles from "./Button.module.scss";

const Button = (props) => {
  const router = useRouter();
  const { description, link, style, id } = props;

  const deleteData = async () => {
    await fetch(`http://localhost:8001/api/bee-yards/${id}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    router.push("/");
  };

  const onClick = () => {
    deleteData();
  };

  return (
    <div className={`${ButtonStyles.button} ${ButtonStyles[style]}`}>
      {link && !id && <Link href={link}>{description}</Link>}
      {id && <div onClick={onClick}>{description}</div>}
      {!link && !id && description}
    </div>
  );
};

export default Button;

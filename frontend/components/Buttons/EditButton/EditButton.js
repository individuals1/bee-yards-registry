import React from "react";
import Button from "../Button";

const EditButton = (props) => {
  const { row } = props;
  return (
    <React.Fragment>
      <Button description={"Edit"} link={`/bee-yards/${row.id}`} />
    </React.Fragment>
  );
};

export default EditButton;

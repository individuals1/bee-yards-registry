import React from "react";
import Button from "../Button";

const AddButton = (props) => {
  const { row } = props;
  return (
    <React.Fragment>
      <Button description={"Add"} link={`/bee-yards/new`} style={"add"} />
    </React.Fragment>
  );
};

export default AddButton;

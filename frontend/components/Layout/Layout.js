import React from "react";

import LayoutStyles from "./Layout.module.scss";

const Layout = (props) => {
  const { component } = props;
  return <div className={LayoutStyles.fullscreen}>{component}</div>;
};

export default Layout;

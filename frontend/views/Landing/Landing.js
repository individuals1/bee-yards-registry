import React, { useEffect, useState } from "react";

import Layout from "../../components/Layout";
import BeeYards from "../../components/BeeYards";

const Landing = () => {
  return <Layout component={<BeeYards />} />;
};

export default Landing;
